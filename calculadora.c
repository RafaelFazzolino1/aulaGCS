#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

int multiplicacao(const int a, const int b) {
  return a*b;
}

int divisao(int a, int b){
	return a/b;

}

int soma(int a, int b){
	return a+b;
}

int subtracao(int a, int b) {
	int resultado = a - b;
	return resultado;
}

int menu(char operacao, int num1, int num2){
	switch(operacao) {
		case '*': return multiplicacao(num1, num2);
			break;
		case '/': return divisao(num1, num2);
			break;
		case '+': return soma(num1, num2);
			break;
		case '-': return subtracao(num1, num2);
			break;
		default: printf("não encontrada\n"); return INT_MIN;
	}
}

int main(){
	char op;
	int a, b;
	printf("EX: 1 + 2\n");

	while (scanf("%d %c %d", &a, &op, &b)) {
		int resultado = menu(op, a, b);
		printf("= %d\n", resultado);
	}

	return 0;
}
